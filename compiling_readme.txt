Get IntelliJ Idea (Java IDE) -- community edition is free.

Open the Cell_Magic_Wand_Tool subdirectory as a project.

You can then Build -> Make Project to compile.

Build -> Build Artifacts will produce a .jar file of the StackStitcher source. That .jar can be used as an ImageJ plugin.

 
